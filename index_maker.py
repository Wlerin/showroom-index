#!/usr/bin/env python3

import json
from bs4 import BeautifulSoup
import argparse
from showroom.api.session import ClientSession as _Session
import re

# this is a mess of functions designed to manage the indices
# it's not meant for distribution

_session = _Session()
_draft_re = re.compile(r'AKB48グループドラフト(\d+)期候補者(\d+)番 ([\w\s]+)$')


'''
def parse_main_text_old(text):
    # gets main-text from create_room
    # returns tuple of jpnName, jpnTeam, engTeam, priority
    try:
            jpn_name, jpn_team = text.strip('）').split('（')
        except ValueError:  # Damn you Renacchi
            jpn_name = jpn_team = eng_team = text
        else:
            replace_dict = {'研究生': 'KKS', 'チーム': "Team ", ' ': ' ',
                            'Ⅱ': 'II', '８': '8', 'ドラフト': "Draft ",
                            "Ⅳ": "IV", '４': '4', 'Ⅲ': 'III',
                            '台湾留学生': ''}

            for key in replace_dict:
                eng_team = eng_team.replace(key, replace_dict[key])
                eng_team = eng_team.strip(' ')

        if 'Team 8' in eng_team or 'Team TII' in eng_team:
            priority = 30
        else:
            priority = 20

        return jpn_name, jpn_team, eng_team, priority
'''

def parse_main_text_new(text):
    # gets main-text from create_room
    # returns tuple of jpnName, jpnTeam, jpnGroup, engTeam, engGroup, priority
    try:
        jpn_name, jpn_team = text.strip('）').split('（')
    except ValueError:  # Damn you Renacchi
        jpn_name = jpn_group = eng_group = text
        jpn_team = eng_team = ""
    else:
        replace_dict = {'研究生': 'KKS', 'チーム': "Team ", ' ': ' ',
                        'Ⅱ': 'II', '８': '8', 'ドラフト': "Draft ",
                        "Ⅳ": "IV", '４': '4', 'Ⅲ': 'III',
                        '台湾留学生': '',
                        '欅坂': 'Keyakizaka', '乃木坂': 'Nogizaka', '日向坂': 'Hinatazaka'}
        
        if jpn_team[3:6] == "48 ":
            jpn_group, jpn_team = jpn_team.split(' ', 1)
        else:
            # TODO: make this the correct japanese word
            jpn_group = 'Other'
        eng_group, eng_team = jpn_group, jpn_team

        for key in replace_dict:
            eng_team = eng_team.replace(key, replace_dict[key])
            eng_team = eng_team.strip(' ')

        if '46' in jpn_team:
            jpn_group, jpn_team, eng_group, eng_team = jpn_team, '', eng_team, ''

    if 'Team 8' in eng_team or 'Team TII' in eng_team or 'zaka' in eng_group:
        priority = 30
    else:
        priority = 20

    return jpn_name, jpn_team, jpn_group, eng_team, eng_group, priority


_sakamichiaudition_re = re.compile(r'\d+$')

def create_room(card, card_type, index_version, western_order=True):
    # div.listcardinfo-image
    #   div.listcardinfo-label (irrelevant)
    #   a.room-url IMPORTANT
    # div.listcardinfo-info
    #   h4.listcardinfo-main-text (jpnName and team, but this varies by event, almost need to make a separate parser...)
    #   p.listcardinfo-sub-text (contains engName, but this is better gotten from room-url
    #      some bio info that i'm not going to save right now
    # div.listcardinfo-menu
    #   a.profile-link IMPORTANT (easiest way to get the room_id?)
    #   a.room-url same as above
    web_url = card.select_one('a.room-url')['href'].strip('/')

    if card_type == "normal":
        if web_url.startswith('48') or web_url.startswith('46'):
            try:
                _, *eng_names = web_url.split('_')
                if western_order:
                    engName = " ".join(eng_names[::-1]).title()
                else:
                    engName = " ".join(eng_names).title()
            except ValueError:
                engName = input('Please type a name for {}'.format(web_url)).title()
        else:
            engName = web_url.replace('_', ' ')
            print('Named {} {}'.format(web_url, engName))

        jpnName, jpnTeam, jpnGroup, engTeam, engGroup, priority = parse_main_text_new(card.select_one('h4.listcardinfo-main-text').text)

    elif card_type == "audition":
        # updated for Sakamichi Auditions
        entry_no = _sakamichiaudition_re.search(web_url).group(0)
        # engGroup, *audition_no, entry_no = web_url.split("_")
        # engGroup = engGroup.upper()
        engGroup = "Sakamichi Audition"
        engName = "Entry No. {:03d}".format(int(entry_no))
        engTeam = ""
        jpnGroup = '坂道合同オーディション'
        jpnName = 'エントリーナンバー{}番'.format(entry_no)
        jpnTeam = ""
        # if len(audition_no) == 1:
        #     audition_no = audition_no[0]
        # else:
        #     audition_no = "1st"
        # engTeam = "{} Gen".format(audition_no)
        # engName = "Entry Number {:03d}".format(int(entry_no))
        priority = 20
        # jpnGroup, jpnTeam, jpnName = card.select_one('h4.listcardinfo-main-text').text.rsplit(' ', 2)

    elif card_type == "draft":
        main_text = card.select_one('h4.listcardinfo-main-text').text
        match = _draft_re.search(main_text)
        if not match:
            print('Failed to match:', main_text)
            gen = '3rd'
            entry_no = 0
            jpnName = ''
        else:
            gen = match.group(1)
            if gen[-1] == '1':
                gen += 'st'
            elif gen[-1] == '2':
                gen += 'nd'
            elif gen[-1] == '3':
                gen += 'rd'
            else:
                gen += 'th'

            entry_no = match.group(2)
            jpnName = match.group(3).strip()
        engGroup = 'AKB48 Group'
        engTeam = jpnTeam =  'Draft ' + gen + ' Gen Auditions'
        engName = ''
        jpnGroup = 'AKB48グループ'
        jpnTeam = 'ドラフト{}期候補者'.format(gen[:-2])

        priority = 20

    elif card_type == "test":
        engTeam = ""
        engGroup = "TEST"
        engName = web_url
        jpnName, jpnTeam, jpnGroup = "", "", ""
        priority = 20

    room_id = card.select_one('a.profile-link')['href'].split('=')[-1]

    if index_version == 1:
        jpnTeam, engTeam = (jpnGroup + ' ' + jpnTeam).strip(), (engGroup + ' ' + engTeam).strip()
        result = {"jpnName": jpnName,
            "engName": engName,
            "jpnTeam": jpnTeam,
            "engTeam": engTeam,
            "priority": priority,
            "room_id": room_id,
            "web_url": 'https://www.showroom-live.com/' + web_url}
    else:
        result = {"jpnName": jpnName,
            "engName": engName,
            "jpnTeam": jpnTeam,
            "engTeam": engTeam,
            "jpnGroup": jpnGroup,
            "engGroup": engGroup,
            "priority": priority,
            "room_id": room_id,
            "web_url": 'https://www.showroom-live.com/' + web_url}

    return result


def get_event_members(target_url, event_name, card_type="normal", index_version=2, extend=False, western_order=False):
    # https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr
    # https://www.showroom-live.com/event/all_rooms?event_id=16026
    # https://www.showroom-live.com/event/all_rooms?event_id=1820 < 16th gen
    # https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr&group=ske48&team=team_4
    # https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr&group=nmb48&team=team_4
    # getting the event id/event rooms manually is bothersome
    # https://www.showroom-live.com/event/room_list?event_id=2499 < stu48
    # https://www.showroom-live.com/event/room_list?event_id=5406 <
    # ul.contentlist-rowlist or ul#list-room_list
    #   div.contentlist-link

    r = _session.get(target_url)
    soup = BeautifulSoup(r.text, 'lxml')
    try:
        rooms = soup.select_one('ul#list-room_list').select('div.contentlist-link')
    except AttributeError:
        try:
            rooms = soup.select_one('ul.contentlist-rowlist').select('div.contentlist-link')
        except AttributeError:
            print('Couldn\'t read {}'.format(target_url))
            return

    event_index = []
    for card in rooms:
        event_index.append(create_room(card, card_type, index_version, western_order=western_order))
    
    print('Added {} rooms'.format(len(event_index)))
    
    if extend:
        try:
            with open('{}.jdex'.format(event_name), encoding='utf8') as infp:
                data = json.load(infp)
                data.extend(event_index)
                event_index = data
        except FileNotFoundError:
            pass
    event_index.sort(key=lambda room: (room['priority'], room['engName']))
    with open('{}.jdex'.format(event_name), 'w', encoding='utf8') as outfp:
        json.dump(event_index, outfp, ensure_ascii=False, indent=2)


def get_event_members2(target_url, event_name, card_type="normal", index_version=2, extend=False, western_order=False):
    """
    This completely ignores the target_url right now and uses a hardcoded event_id

    It kind of needs to be that way since every event is a little different. But still.
    """
    s = _session
    next_page = 1

    event_index = []
    while next_page:
        r = s.json('https://www.showroom-live.com/event/room_list?event_id=9694&p={}'.format(next_page))
        next_page = r['next_page']
        page_text = r['html']
        soup = BeautifulSoup(page_text, 'lxml')
        try:
            rooms = soup.select('div.listcardinfo')
        except AttributeError:
            print('Couldn\'t read page {}'.format(next_page - 1))
            return
        for card in rooms:
            event_index.append(create_room(card, card_type, index_version, western_order=western_order))

    print('Added {} rooms'.format(len(event_index)))

    if extend:
        try:
            with open('{}.jdex'.format(event_name), encoding='utf8') as infp:
                data = json.load(infp)
                data.extend(event_index)
                event_index = data
        except FileNotFoundError:
            pass
    event_index.sort(key=lambda room: (room['priority'], room['engName']))
    with open('{}.jdex'.format(event_name), 'w', encoding='utf8') as outfp:
        json.dump(event_index, outfp, ensure_ascii=False, indent=2)


def get_campaign_members(target_url, campaign_name, card_type="normal", extend=False, western_order=False):
    s = _session
    r = s.get(target_url)
    soup = BeautifulSoup(r.text, 'lxml')
    index = []
    for section in soup.select('ul.room-list'):
        heading = section.select_one('h3.contentlist-heading').text

        # this won't work on Nogi gdi
        try:
            group, team = heading.split(' ', 1)
        except ValueError:
            if '46' in heading:
                group = heading.split('Room')[0]
                team = ''
            else:
                group = '乃木坂46'
                team = heading
        team = team.split('Room')[0]

        replace_dict = {'二期生': '2nd Gen', '1期生': '1st Gen', '2期生': '2nd Gen', '4期生': '4th Gen'}

        for key in replace_dict:
            team = team.replace(key, replace_dict[key])
            team = team.strip(' ')

        rooms = section.select('div.contentlist-link')
        
        for card in rooms:
            room = create_room(card, card_type, index_version=2, western_order=western_order)
            room['jpnTeam'] = team
            if room['jpnGroup'] != group:
                print('mismatched groups for', room['engName'])
            index.append(room)

    index.sort(key=lambda room: (room['jpnTeam'], room['priority'], room['engName']))
    with open('{}.jdex'.format(campaign_name), 'w', encoding='utf8') as outfp:
        json.dump(index, outfp, ensure_ascii=False, indent=2)


def get_campaign_members2(target_url, campaign_name, card_type="normal", extend=False, western_order=False):
    s = _session
    r = s.get(target_url)
    soup = BeautifulSoup(r.text, 'lxml')
    index = []
    for card in soup.select('div.room-card'):
        room = create_room(card, card_type, index_version=2, western_order=western_order)
        room['jpnTeam'] = None
        index.append(room)
    index.sort(key=lambda room: (room['jpnTeam'], room['priority'], room['engName']))
    with open('{}.jdex'.format(campaign_name), 'w', encoding='utf8') as outfp:
        json.dump(index, outfp, ensure_ascii=False, indent=2)


def build_parser():
    parser = argparse.ArgumentParser(description="Generates new jdex files based on event or campaign pages",
                                     epilog="")
    parser.add_argument('url', type=str,
                        help='Url of a campaign or event page room list')
    parser.add_argument('name', type=str,
                        help='Name for the new index')
    parser.add_argument('--index-version', type=int, choices=[1,2], default=2,
                        help='Version of index to use (1 for old version with fused group and team'
                        '2 for new version with separate groups and teams)')
    parser.add_argument('--card-type', type=str, choices=['normal', 'audition', 'draft', 'test'], default='normal',
                        help='Type of room to expect. Don\'t use this, just leave it as normal.')
    parser.add_argument('--extend',  action='store_true', help='Whether to add rooms on to an existing file.'
                        'You probably don\'t want to do this.')
    parser.add_argument('--western-order', action='store_true', help='Whether member names in the url are in western order')
    return parser

def parse_urlkey(urlkey, western_order):
    if urlkey.startswith('48') or urlkey.startswith('46'):
        try:
            _, *eng_names = urlkey.split('_')
            if western_order:
                engName = " ".join(eng_names[::-1]).title()
            else:
                engName = " ".join(eng_names).title()
        except ValueError:
            engName = input('Please type a name for {}'.format(urlkey)).title()
    else:
        engName = urlkey.replace('_', ' ')
        print('Named {} {}'.format(urlkey, engName))
    return engName

def create_room2(card, western_order=True):
	urlkey = card['url_key']
	jpnName = card['name'].split('（')[0]
	engName = parse_urlkey(urlkey, western_order)
	return dict(
		engName=engName,
		jpnName=jpnName,
		engGroup="NMB48",
		jpnGroup="NMB48",
		engTeam="",
		jpnTeam="",
		web_url=f'https://www.showroom-live.com/{urlkey}',
		room_id=card['id'],
		priority=30
	)


def main():
    parser = build_parser()
    args = parser.parse_args()

    # get_event_members2(args.url, args.name, args.card_type, args.index_version, args.extend, args.western_order)
    # get_event_members(args.url, args.name, args.card_type, args.index_version, args.extend, args.western_order)
    get_campaign_members2(args.url, args.name, args.card_type, args.extend, args.western_order)


if __name__ == "__main__":
    # get_event_members("https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr", "akb48_sr")
    # get_event_members("https://www.showroom-live.com/event/ske48_8th_audition", "ske48_8th", card_type="audition")
    # get_event_members("https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr&group=ske48&team=team_4", "ske48_8th")
    # get_event_members("https://www.showroom-live.com/campaign/all_rooms?title=akb48_sr&group=nmb48", "nmb48_update")
    # https://www.showroom-live.com/event/akbdraft3rd_audition

    '''
    urls = ["https://www.showroom-live.com/event/divinetimechange_final",
            "https://www.showroom-live.com/event/kaminoma_vol4",
            "https://www.showroom-live.com/event/sir_6th_audition",
            "https://www.showroom-live.com/event/welcome-listener2",
            "https://www.showroom-live.com/audition/stardust_2017/",
            "https://www.showroom-live.com/event/owarai_survival_27",
            "https://www.showroom-live.com/event/niija_4",
            "https://www.showroom-live.com/event/bandainamco_cucuri",
            "https://www.showroom-live.com/event/hosi1701",
            "https://www.showroom-live.com/event/nijigenka2"]
    https://www.showroom-live.com/campaign/keyakizaka46_sr
    '''
    # TODO: put the default values in constants at the top of the file
    # main()
    # get_campaign_members2('https://campaign.showroom-live.com/akb48_sr/#/rooms/nmb48/0', 'nmb48_fetch', western_order=True)
    pass
